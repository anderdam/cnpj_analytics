import random
import time

from datetime import datetime
from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType, StringType, DoubleType, FloatType, DecimalType, DateType, TimestampType, BooleanType

from pyspark.sql import SparkSession


today = datetime.now().strftime("%YY%mm%dd")

# Create a SparkSession
spark = SparkSession\
    .builder\
    .appName("pyspark-notebook")\
    .master("local[*]")\
    .config("spark.executor.memory", "2G")\
    .config("spark.driver.memory", "2G")\
    .getOrCreate()

spark.sparkContext.setLogLevel("ERROR")
spark.sparkContext.jvm.System.gc()

# Carregando os dados
df_cnaes = spark.read.csv('dataset/CNAECSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'cnaeId')\
    .withColumnRenamed('_c1', 'descricao')
df_cnaes.printSchema()

df_empresas = spark.read.csv('dataset/EMPRECSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'cnpjId')\
    .withColumnRenamed('_c1', 'razao-social')\
    .withColumnRenamed('_c2', 'natureza_juridica')\
    .withColumnRenamed('_c3', 'qualificacao_responsavel')\
    .withColumnRenamed('_c4', 'capital_social').fillna(0)\
    .withColumnRenamed('_c5', 'porte')\
    .withColumnRenamed('_c6', 'ente_federativo_responsavel')

df_empresas = df_empresas.withColumn('capital_social', f.regexp_replace('capital_social', ',', '.'))
df_empresas = df_empresas.withColumn('capital_social', df_empresas['capital_social'].cast(DoubleType()))

df_empresas.printSchema()
df_empresas.show(5)

df_estabelecimentos = spark.read.csv('dataset/ESTABELE', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'cnpjId')\
    .withColumnRenamed('_c1', 'cnpj_ordem')\
    .withColumnRenamed('_c2', 'cnpj_dv')\
    .withColumnRenamed('_c3', 'identificador_matriz_filial')\
    .withColumnRenamed('_c4', 'nome_fantasia')\
    .withColumnRenamed('_c5', 'situacao_cadastral')\
    .withColumnRenamed('_c6', 'data_situacao_cadastral')\
    .withColumnRenamed('_c7', 'motivo_situacao_cadastral')\
    .withColumnRenamed('_c8', 'nome_cidade_exterior')\
    .withColumnRenamed('_c9', 'codigo_pais')\
    .withColumnRenamed('_c10', 'data_inicio_atividade')\
    .withColumnRenamed('_c11', 'cnaeId')\
    .withColumnRenamed('_c12', 'cnaeId_secundário')\
    .withColumnRenamed('_c13', 'tipo_logradouro')\
    .withColumnRenamed('_c14', 'logradouro')\
    .withColumnRenamed('_c15', 'numero')\
    .withColumnRenamed('_c16', 'complemento')\
    .withColumnRenamed('_c17', 'bairro')\
    .withColumnRenamed('_c18', 'cep')\
    .withColumnRenamed('_c19', 'uf')\
    .withColumnRenamed('_c20', 'codigo_municipio')\
    .withColumnRenamed('_c21', 'ddd1')\
    .withColumnRenamed('_c22', 'telefone1')\
    .drop('_c23')\
    .drop('_c24')\
    .drop('_c25')\
    .drop('_c26')\
    .withColumnRenamed('_c27', 'email')\
    .withColumnRenamed('_c28', 'situacao_especial')\
    .withColumnRenamed('_c29', 'data_situacao_especial')\
    .fillna('n/a')
df_estabelecimentos.printSchema()
df_estabelecimentos.show(5)


df_socios = spark.read.csv('dataset/SOCIOCSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'cnpjId')\
    .withColumnRenamed('_c1', 'identificador_socio')\
    .withColumnRenamed('_c2', 'nome_socio')\
    .withColumnRenamed('_c3', 'cnpj_cpf_socio')\
    .withColumnRenamed('_c4', 'codigo_qualificacao_socio')\
    .withColumnRenamed('_c5', 'dt_entrada_sociedade')\
    .withColumnRenamed('_c6', 'pais')\
    .withColumnRenamed('_c7', 'representante_legal')\
    .withColumnRenamed('_c8', 'nome_representante')\
    .withColumnRenamed('_c9', 'qualificacao_representante_legal')\
    .withColumnRenamed('_c10', 'faixa_etaria')
df_socios.fillna('n/a')
df_socios.printSchema()

df_simples = spark.read.csv('dataset/SIMPLES', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'cnpjId')\
    .withColumnRenamed('_c1', 'opcao_simples')\
    .withColumnRenamed('_c2', 'data_opcao_simples')\
    .withColumnRenamed('_c3', 'data_exclusao_simples')\
    .withColumnRenamed('_c4', 'opcao_mei')\
    .withColumnRenamed('_c5', 'data_opcao_mei')\
    .withColumnRenamed('_c6', 'data_exclusao_mei')\
    .fillna('n/a')
df_simples.printSchema()

df_paises = spark.read.csv('dataset/PAISCSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'paisId')\
    .withColumnRenamed('_c1', 'nome_pais')\
    .fillna('n/a')
df_paises.printSchema()

df_municipios = spark.read.csv('dataset/MUNICCSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'municipioId')\
    .withColumnRenamed('_c1', 'nome_municipio')\
    .fillna('n/a')
df_municipios.printSchema()

df_qualificacoes_socios = spark.read.csv('dataset/QUALSCSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'qualificacaoId')\
    .withColumnRenamed('_c1', 'descricao')
df_qualificacoes_socios.fillna('n/a')
df_qualificacoes_socios.printSchema()

df_naturezas_juridicas = spark.read.csv('dataset/NATJUCSV', inferSchema=True, sep=';', encoding='utf-8')\
    .withColumnRenamed('_c0', 'naturezaId')\
    .withColumnRenamed('_c1', 'descricao')
df_naturezas_juridicas.fillna('n/a')
df_naturezas_juridicas.printSchema()

# Convertendo para parquet
# df_cnaes.write.parquet('parquet_dataset/CNAECSV.parquet')
# df_empresas.write.parquet('parquet_dataset/EMPRECSV.parquet', partitionBy='natureza_juridica')
# df_estabelecimentos.write.parquet('parquet_dataset/ESTABELE.parquet')
# df_naturezas_juridicas.write.parquet('parquet_dataset/NATJUCSV.parquet')
# df_qualificacoes_socios.write.parquet('parquet_dataset/QUALSCSV.parquet')
# df_municipios.write.parquet('parquet_dataset/MUNICCSV.parquet')
# df_simples.write.parquet('parquet_dataset/SIMPLES.parquet')
# df_paises.write.parquet('parquet_dataset/PAISCSV.parquet')
# df_socios.write.parquet('parquet_dataset/SOCIOCSV.parquet')


# Quantidade de empresas
# df_empresas_count = df_empresas.count()
# print(df_empresas_count)


